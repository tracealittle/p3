package Controller;

import Boundary.BusinessUI;
import javax.swing.JFrame;
import Entity.User;
import Entity.Business;
import Boundary.PublicUI;
import Boundary.BusinessUI;
import Boundary.HealthcareStaffUI;

public class LoginController {

    public boolean isLogin(String username, String password, JFrame frame) {
        User user = new User();
        if (user.login(username, password, frame)) {
            if (user.getRole().equals("user")) {
                PublicUI dashboard = new PublicUI(user.getID(), user.getName());
                dashboard.setVisible(true);
                dashboard.pack();
                dashboard.setLocationRelativeTo(null);
                dashboard.lblUsername.setText(username);
            }
            if (user.getRole().equals("business")) {
                BusinessUI dashboard = new BusinessUI(user.getID(), user.getName());
                dashboard.setVisible(true);
                dashboard.pack();
                dashboard.setLocationRelativeTo(null);
                dashboard.lblUsername.setText(username);
            }
            if (user.getRole().equals("healthcareStaff")) {
                HealthcareStaffUI dashboard = new HealthcareStaffUI(user.getID(), user.getName());
                dashboard.setVisible(true);
                dashboard.pack();
                dashboard.setLocationRelativeTo(null);
                dashboard.lblUsername.setText(username);
            }/*
            if (user.getRole().equals("healthcare organisation")){
                    HealthcareOrganisationUI dashboard = new HealthcareOrganisationUI();
                    dashboard.setVisible(true);
                    dashboard.pack();
                    dashboard.setLocationRelativeTo(null);
                    dashboard.lblUsername.setText(username);
            }*/

            frame.dispose();
            return true;
        } else {
            return false;
        }
    }
}
