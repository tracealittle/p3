package Controller;

import java.util.ArrayList;
import Entity.Business;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ViewSelectedVisitorController {

    public void fillTable(int id, JTable table, String date) {
        Business user = new Business();
        user.fillSelectedViewVisitors(id, date);
        ArrayList<Object[]> o = user.getSelectedVisitor();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        for (int i = 0; i < o.size(); i++) {
            model.addRow(o.get(i));
        }
    }
}
