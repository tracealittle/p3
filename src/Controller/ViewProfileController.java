package Controller;

import Entity.User;
import java.util.ArrayList;

public class ViewProfileController {

    public ArrayList<String> fillTable(int id) {
        User user = new User();
        user.fillProfile(id);
        return user.getProfile();
    }

}
