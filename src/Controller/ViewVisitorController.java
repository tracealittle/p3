package Controller;

import java.util.ArrayList;
import Entity.Business;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ViewVisitorController {

    public void fillTable(int id, JTable table) {
        Business user = new Business();
        user.fillViewVisitors(id);
        ArrayList<Object[]> o = user.getVisitor();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        for (int i = 0; i < o.size(); i++) {
            model.addRow(o.get(i));
        }
    }
}
