package Controller;

import java.util.ArrayList;
import Entity.Alert;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ViewAlertStatusController {

    public void fillTable(JTable table) {
        Alert alert = new Alert();
        alert.fillAlertStatus();
        ArrayList<Object[]> o = alert.getAlertStatus();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        for (int i = 0; i < o.size(); i++) {
            model.addRow(o.get(i));
        }
    }
}
