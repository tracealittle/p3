package Controller;

import java.util.ArrayList;
import Entity.HealthcareStaff;
import Entity.User;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class SearchUserController {

    public void fillTable(String name, JTable table) {
        HealthcareStaff user = new HealthcareStaff();
        user.fillSearch(name);
        ArrayList<Object[]> o = user.getSearchUser();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        for (int i = 0; i < o.size(); i++) {
            model.addRow(o.get(i));
        }
    }
    
    public void updateUserMedical (int id, int vaccine, String covid, JFrame frame){
        User user = new User(id, frame);
        if (user.getVaccine() != vaccine){
            user.setVaccine(vaccine, frame);  
        }
        if (user.getCovidstatus().equals(covid) != true){
            user.setCovidstatus(covid, frame);
            if (covid.equals("Positive")){
                user.alertBusiness (id, frame);
            } else {
                JOptionPane.showMessageDialog(frame, "User's Covid status has been updated!");
            }
        }
    }
}
