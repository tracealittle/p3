package Controller;

import java.util.ArrayList;
import Entity.User;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class PastPlacesController {

    public void fillTable(int id, JTable table, JFrame frame) {
        User user = new User();
        user.fillPastPlaces(id, frame);
        ArrayList<Object[]> o = user.getPastPlaces();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        for (int i = 0; i < o.size(); i++) {
            model.addRow(o.get(i));
        }
    }
}
