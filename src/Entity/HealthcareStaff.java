package Entity;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;

public class HealthcareStaff {

    private String Username;
    private String Password;
    private int ID;
    private String Role;
    private ArrayList<Object[]> SearchUser = new ArrayList<Object[]>();

    public String getName() {
        return Username;
    }

    public int getID() {
        return ID;
    }

    public String getRole() {
        return Role;
    }

    public ArrayList<Object[]> getSearchUser() {
        return SearchUser;
    }

    public void fillSearch(String name) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT u.ID, u.name, u.covidstatus, vc.certificate FROM user u join vaccinecertificate vc on u.vaccine = vc.id WHERE name like '%"
                    + name + "%' Order by id;";

            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Object[] o = {resultSet.getInt("ID"), resultSet.getString("name"), resultSet.getString("certificate"), resultSet.getString("covidstatus")};
                SearchUser.add(o);
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }

    }

    public void updateUser(String name, int vaccine, String covidstatus) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "UPDATE user SET vaccine='" + vaccine + "', covidstatus = '" + covidstatus + "' WHERE name ='" + name + "';";

            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);

            preparedStatement.executeUpdate();

        } catch (Exception exception) {
            System.out.println(exception);
        }

    }

}
