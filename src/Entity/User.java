package Entity;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;

public class User {

    private String Username;
    private String Password;
    private int ID;
    private String Role;
    private int vaccine;
    private String covidstatus;
    private boolean suspend;
    private ArrayList<Object[]> PastPlaces = new ArrayList<Object[]>();
    private ArrayList<String> Profile = new ArrayList<String>();
    
    public User (){}
    
    public User (int id, JFrame frame){
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT * FROM user WHERE ID = '" + id + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Username = resultSet.getString("name");
                Password = resultSet.getString("password");
                ID = resultSet.getInt("ID");
                Role = resultSet.getString("role");
                vaccine = resultSet.getInt("vaccine");
                covidstatus = resultSet.getString("covidstatus");
                suspend = resultSet.getBoolean("suspend");
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }
    }
    
    public String getName() {
        return Username;
    }

    public int getID() {
        return ID;
    }

    public String getRole() {
        return Role;
    }
    
    public int getVaccine() {
        return vaccine;
    }
    
    public String getCovidstatus() {
        return covidstatus;
    }

    public ArrayList<Object[]> getPastPlaces() {
        return PastPlaces;
    }

    public ArrayList<String> getProfile() {
        return Profile;
    }

    public boolean login(String username, String password, JFrame frame) {
        boolean result = false;

        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT name, password, id, role FROM user WHERE name = '"
                    + username
                    + "' AND password = '"
                    + password + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Username = resultSet.getString("name");
                Password = resultSet.getString("password");
                ID = resultSet.getInt("ID");
                Role = resultSet.getString("role");
                result = true;
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }

        return result;
    }

    public void fillProfile(int ID) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery = "SELECT name, gender, phone, certificate, vaccinedate "
                    + "from user join vaccinecertificate ON user.vaccine = "
                    + "vaccinecertificate.id where user.id = '" + ID + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Profile.add(resultSet.getString("name"));
                Profile.add(resultSet.getString("gender"));
                Profile.add(resultSet.getString("phone"));
                Profile.add(resultSet.getString("certificate"));
                if (resultSet.getDate("vaccinedate") == null){
                    Profile.add("-");
                }else {
                    Profile.add(resultSet.getDate("vaccinedate").toString());
                }
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void fillPastPlaces(int ID, JFrame frame) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT user.name, locationdb.date from locationdb join user"
                    + " on locationdb.businessID = user.ID where locationdb.visitorID = '"
                    + ID + "' Order by date desc";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Object[] o = {resultSet.getString("date"), resultSet.getString("name")};
                PastPlaces.add(o);
            }

        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }
    }
    
    public void setVaccine (int vaccine, JFrame frame) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    =   "update user set vaccine = '" + vaccine + "', vaccinedate = '" + 
                        java.time.LocalDate.now() + "' where ID = '" + ID + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet > 0){
                this.vaccine = vaccine;
                JOptionPane.showMessageDialog(frame, "User's vaccine info has been updated!");
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }
    }
    
    public void setCovidstatus (String covidstatus, JFrame frame) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "update user set covidstatus = '" + covidstatus + "' where ID = '" + ID + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet > 0){
                this.covidstatus = covidstatus;
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }
    }
    
    public void alertBusiness (int id, JFrame frame){
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT DISTINCT user.name, user.ID FROM locationdb JOIN user ON locationdb.businessID = user.ID  "
                    + "WHERE locationdb.visitorID = '"+ id +"' AND Date = '" + java.time.LocalDate.now() + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            int i = 0;
            String info = "";
            while (resultSet.next()) {
                int vid = resultSet.getInt("ID");
                mySqlQuery =    "INSERT  alert (businessID, date, visitorID) " +
                                "SELECT  '" + vid + "' , '" + java.time.LocalDate.now() +"', '" + id + "'" +
                                "WHERE   NOT EXISTS (   SELECT  1 FROM alert " +
                                "WHERE   businessID = '" + vid + "' AND  date = '" + java.time.LocalDate.now() +"')";
                preparedStatement = myConn.prepareStatement(mySqlQuery);
                preparedStatement.executeUpdate();
                i++;
                info = info + String.format("%n%d. %s", i, resultSet.getString("name"));
                mySqlQuery = "update user set covidstatus = 'Exposed' where ID in (select distinct visitorID "
                    + "from locationdb where businessID = '" + resultSet.getInt("ID") + "' and Date = '" + 
                    java.time.LocalDate.now() + "') and not covidstatus = 'Positive'";
                preparedStatement = myConn.prepareStatement(mySqlQuery);
                preparedStatement.executeUpdate();                
            }
            JOptionPane.showMessageDialog(frame, "Alert has been sent to " + i + "business(es):" + info);
            
            
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }
    }
}
