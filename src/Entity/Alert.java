package Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Alert {
  
    private ArrayList<Object[]> ViewAlertStatus = new ArrayList<Object[]>();

    public ArrayList<Object[]> getAlertStatus() {
        return ViewAlertStatus;
    }
    
    
    public void fillAlertStatus() {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT user.name , alert.date, alert.acknowledged\n"
                    + "from alert join user on alert.businessID = user.id\n";

            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Object[] o = {resultSet.getString("date"), resultSet.getString("name"), resultSet.getBoolean("acknowledged")};
                ViewAlertStatus.add(o);
            }
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }
    
   
}
