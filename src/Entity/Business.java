package Entity;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;

public class Business {

    private String Username;
    private String Password;
    private int ID;
    private String Role;
    private ArrayList<Object[]> ViewVisitor = new ArrayList<Object[]>();
    private ArrayList<Object[]> ViewSelectedVisitor = new ArrayList<Object[]>();
    private ArrayList<Object[]> ViewAlert = new ArrayList<Object[]>();

    public String getName() {
        return Username;
    }

    public int getID() {
        return ID;
    }

    public String getRole() {
        return Role;
    }

    public ArrayList<Object[]> getVisitor() {
        return ViewVisitor;
    }

    public ArrayList<Object[]> getSelectedVisitor() {
        return ViewSelectedVisitor;
    }

    public ArrayList<Object[]> getAlert() {
        return ViewAlert;
    }

    public boolean login(String username, String password, JFrame frame) {
        boolean result = false;

        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT name, password, ID, role FROM user WHERE name = '"
                    + username
                    + "' AND password = '"
                    + password + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Username = resultSet.getString("name");
                Password = resultSet.getString("password");
                ID = resultSet.getInt("ID");
                Role = resultSet.getString("role");
                result = true;
            }
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(frame, "Database error: " + exception.getMessage());
        }

        return result;
    }

    public void fillViewVisitors(int ID) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT locationdb.date, count(locationdb.visitorID) as counter "
                    + "from locationdb join user on locationdb.visitorID = user.ID where user.role = 'user' and "
                    + "locationdb.businessID = '" + ID + "' group by date Order by date desc";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Object[] o = {resultSet.getString("date"), resultSet.getString("counter")};
                ViewVisitor.add(o);
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void fillSelectedViewVisitors(int ID, String date) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "SELECT  locationdb.date, locationdb.visitorID, user.name, locationdb.businessID\n"
                    + "from locationdb join user on locationdb.visitorID = user.ID \n"
                    + "where locationdb.businessID =  '" + ID + "'\n"
                    + "and user.role = 'user' and locationdb.date = '" + date + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            int vaccine;

            while (resultSet.next()) {

                Object[] o = {resultSet.getString("locationdb.visitorID"), resultSet.getString("user.name")};
                ViewSelectedVisitor.add(o);
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void fillAlert(int ID) {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    = "select alert.Date , user.name, user.covidstatus, alert.acknowledged \n" +
                    "from alert join user on user.ID = alert.visitorID \n" +
                    "where alert.acknowledged = 0 \n" +
                    "and alert.businessID = '" + ID + "'\n";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Object[] o = {resultSet.getString("Date"), resultSet.getString("name"), resultSet.getString("covidstatus")};
                ViewAlert.add(o);
            }

        } catch (Exception exception) {
            System.out.println(exception);
        }
    }
    
     public void acknowledgeAlertButton(int ID)
    {
        try {
            Connection myConn = MYSQLConnection.getConnection();
            String mySqlQuery
                    =  "update alert\n" +
                        "SET acknowledged = 1 \n" +
                        "where businessID =  '" + ID + "'";
            PreparedStatement preparedStatement = myConn.prepareStatement(mySqlQuery);
            int resultSet = preparedStatement.executeUpdate(mySqlQuery);
         }
         catch (Exception exception) {
            System.out.println(exception);
        }
    }
    
    
}
