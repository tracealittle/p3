package Entity;

import java.sql.*;

/**
 *
 * @author user
 */
public class MYSQLConnection {

    public static Connection getConnection() throws Exception {
        String dbRoot = "jdbc:mysql://";
        String hostName = "localhost:3306/";
        String dbName = "tracealittle";
        String dbUrl = dbRoot + hostName + dbName;

        String hostUsername = "root";
        String hostPassword = "root";
        Connection myConn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            myConn = (Connection) DriverManager.getConnection(dbUrl, hostUsername, hostPassword);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myConn;
    }

}
